package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"log"
	"os"
)

// Globals
var awsTag string
var sess *session.Session
var ec2svc *ec2.EC2

type MyEvent struct {
	Action string `json:"action"`
}

func init() {
	// Get the tag we'll be searching for
	awsTag = os.Getenv("AWS_TAG")

	// Start AWS Session, default to what I named my profile
	sess, err := session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		log.Fatal(err)
	} else {
		// Create EC2 service from session
		ec2svc = ec2.New(sess)
	}
}

func HandleRequest(ctx context.Context, action MyEvent) (bool, error) {
	instanceFilter := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			{
				Name: aws.String("tag-key"),
				Values: []*string{
					aws.String("Name"),
				},
			},
		},
	}
	// Get all EC2 instances
	instancesResp, err := ec2svc.DescribeInstances(instanceFilter)
	if err != nil {
		log.Fatal("Error received when listing EC2 instances: %s", err)
	}

	reservations := instancesResp.Reservations
	var instances []*ec2.Instance
	for _, resv := range reservations {
		for _, inst := range resv.Instances {
			instances = append(instances, inst)
		}
	}

	log.Println("Got instance count ", len(instances))

	log.Println(fmt.Sprintf("Going to %s instances tagged with %s.", action.Action, awsTag))

	var instanceIds []*string
	for _, inst := range instances {
		instanceIds = append(instanceIds, inst.InstanceId)
	}
	log.Printf("%s", instanceIds)

	switch action.Action {
	case "start":
		_, err = ec2svc.StartInstances(&ec2.StartInstancesInput{
			InstanceIds: instanceIds,
		})
	case "stop":
		_, err = ec2svc.StopInstances(&ec2.StopInstancesInput{
			InstanceIds: instanceIds,
		})
	default:
		log.Fatal("%s is not a valid action. Must be start or stop", action.Action)
	}

	if err != nil {
		log.Fatal(err)
		return false, err
	} else {
		return true, nil
	}
}

func main() {
	lambda.Start(HandleRequest)
}
